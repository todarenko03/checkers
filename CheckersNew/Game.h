#pragma once
#include "SFML/Graphics.hpp"
#include <vector>;

class Game {

private:
	int init_field[8][8] = { {-1, 2, -1, 2, -1, 2, -1, 2},
							{2, -1, 2, -1, 2, -1, 2, -1},
							{-1, 2, -1, 2, -1, 2, -1, 2},
							{-2, -1, -2, -1, -2, -1, -2, -1},
							{-1, -2, -1, -2, -1, -2, -1, -2},
							{1, -1, 1, -1, 1, -1, 1, -1},
							{-1, 1, -1, 1, -1, 1, -1, 1},
							{1, -1, 1, -1, 1, -1, 1, -1} };

public:
	int field[8][8] = { {-1, 2, -1, 2, -1, 2, -1, 2},
						{2, -1, 2, -1, 2, -1, 2, -1},
						{-1, 2, -1, 2, -1, 2, -1, 2},
						{-2, -1, -2, -1, -2, -1, -2, -1},
						{-1, -2, -1, -2, -1, -2, -1, -2},
						{1, -1, 1, -1, 1, -1, 1, -1},
						{-1, 1, -1, 1, -1, 1, -1, 1},
						{1, -1, 1, -1, 1, -1, 1, -1} };
	int size = 50;
	sf::Texture t1, t2, t3, t4, t5;
	sf::Sprite f[48];
	int kings[24];
	int step = 1;
	std::vector<int> whites_are_attack, blacks_are_attack;
	void load_textures();
	void load_position();
	bool legal_move(int checker, int step, int nx, int ny, int ox, int oy);
	bool is_attack(int checker, int x, int y);
	bool are_attack(int step);
	bool is_attacked(int checker, int step, int ox, int oy, int nx, int ny);
	void attacked(int checker, int step, int ox, int oy, int nx, int ny);
	void updateVectorOfAttackingCheckers();
	void makeACheckerKing(int &n);
	int someone_wins();
};