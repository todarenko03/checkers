#include "Game.h"

void Game::load_textures() {

	t1.loadFromFile("Images/white.png");
	t2.loadFromFile("Images/black.png");
	t3.loadFromFile("Images/field.png");
	t4.loadFromFile("Images/whiteKing.png");
	t5.loadFromFile("Images/blackKing.png");

	for (int i = 0; i < 48; i++) {

		if (i >= 36) {
			f[i].setTexture(t4);
		}

		else if (i >= 24) {
			f[i].setTexture(t5);
		}

		else if (i >= 12) {
			f[i].setTexture(t1);
		}

		else {
			f[i].setTexture(t2);
		}
	}
}

void Game::load_position() {

	int k = 0;

	for (int i = 0; i < 8; i++) {

		for (int j = 0; j < 8; j++) {

			int n = init_field[i][j];

			if ((n == -1) || (n == -2)) continue;

			if ((n == 1) || (n == 2)) {

				f[k].setPosition(size * j, size * i);
				field[i][j] = k;
				k++;
			}
		}
	}

	for (int i = 24; i < 48; i++) {

		f[i].setPosition(-100, -100);
	}
}

bool Game::legal_move(int checker, int step, int nx, int ny, int ox, int oy) {

	if (step == 2) {

		if (checker < 12 && field[nx][ny] == -2 && (ny == oy + 1 || ny == oy - 1) && nx == ox + 1) {
			return true;
		}

		else if (checker >= 24 && checker < 36 && field[nx][ny] == -2 && (abs(nx - ox) == abs(ny - oy))) {
			return true;
		}
	}

	else if (step == 1) {

		if (checker >= 12 && checker < 24 && field[nx][ny] == -2 && (ny == oy + 1 || ny == oy - 1) && nx == ox - 1)
		{
			return true;
		}

		else if (checker >= 36 && field[nx][ny] == -2 && (abs(nx - ox) == abs(ny - oy))) {
			return true;
		}
	}

	return false;
}

bool Game::is_attack(int checker, int x, int y) {
	if (checker < 12) {

		if (x <= 5 && y <= 5) {
			if (((field[x + 1][y + 1] >= 12 && field[x + 1][y + 1] < 24) || field[x + 1][y + 1] >= 36) && field[x + 2][y + 2] == -2) {
				return true;
			}
		}

		if (x >= 2 && y >= 2) {
			if (((field[x - 1][y - 1] >= 12 && field[x - 1][y - 1] < 24) || field[x - 1][y - 1] >= 36) && field[x - 2][y - 2] == -2) {
				return true;
			}
		}

		if (x <= 5 && y >= 2) {
			if (((field[x + 1][y - 1] >= 12 && field[x + 1][y - 1] < 24) || field[x + 1][y - 1] >= 36) && field[x + 2][y - 2] == -2) {
				return true;
			}
		}

		if (x >= 2 && y <= 5) {
			if (((field[x - 1][y + 1] >= 12 && field[x - 1][y + 1] < 24) || field[x - 1][y + 1] >= 36) && field[x - 2][y + 2] == -2) {
				return true;
			}
		}
	}

	else if (checker < 24) {

		if (x <= 5 && y <= 5) {
			if (((field[x + 1][y + 1] < 12 && field[x + 1][y + 1] > -1) || (field[x + 1][y + 1] >= 24 && field[x + 1][y + 1] < 36)) && field[x + 2][y + 2] == -2) {
				return true;
			}
		}

		if (x >= 2 && y >= 2) {
			if (((field[x - 1][y - 1] < 12 && field[x - 1][y - 1] > -1) || (field[x - 1][y - 1] >= 24 && field[x - 1][y - 1] < 36)) && field[x - 2][y - 2] == -2) {
				return true;
			}
		}

		if (x <= 5 && y >= 2) {
			if (((field[x + 1][y - 1] < 12 && field[x + 1][y - 1] > -1) || (field[x + 1][y - 1] >= 24 && field[x + 1][y - 1] < 36)) && field[x + 2][y - 2] == -2) {
				return true;
			}
		}

		if (x >= 2 && y <= 5) {
			if (((field[x - 1][y + 1] < 12 && field[x - 1][y + 1] > -1) || (field[x - 1][y + 1] >= 24 && field[x - 1][y + 1] < 36)) && field[x - 2][y + 2] == -2) {
				return true;
			}
		}
	}

	else if (checker < 36) {

		int i = x + 1;
		int j = y + 1;
		bool is_checker_was_attacked = false;

		while (i <= 6 && j <= 6) {

			if (!is_checker_was_attacked && (field[i][j] >= -1 && field[i][j] < 12) || (field[i][j] >= 24 && field[i][j] < 36)) {

				break;
			}

			if ((field[i][j] >= 12 && field[i][j] < 24) || field[i][j] >= 36) {

				is_checker_was_attacked = true;

				if (field[i + 1][j + 1] != -2) {

					is_checker_was_attacked = false;
					break;
				}
			}

			i++;
			j++;
		}

		if (is_checker_was_attacked) {
			return true;
		}

		i = x - 1;
		j = y - 1;

		is_checker_was_attacked = false;

		while (i >= 1 && j >= 1) {

			if (!is_checker_was_attacked && (field[i][j] >= -1 && field[i][j] < 12) || (field[i][j] >= 24 && field[i][j] < 36)) {

				break;
			}

			if ((field[i][j] >= 12 && field[i][j] < 24) || field[i][j] >= 36) {

				is_checker_was_attacked = true;

				if (field[i - 1][j - 1] != -2) {

					is_checker_was_attacked = false;
					break;
				}
			}

			i--;
			j--;
		}

		if (is_checker_was_attacked) {

			return true;
		}

		i = x + 1;
		j = y - 1;
		is_checker_was_attacked = false;

		while (i <= 6 && j >= 1) {

			if (!is_checker_was_attacked && (field[i][j] >= -1 && field[i][j] < 12) || (field[i][j] >= 24 && field[i][j] < 36)) {

				break;
			}

			if ((field[i][j] >= 12 && field[i][j] < 24) || field[i][j] >= 36) {

				is_checker_was_attacked = true;

				if (field[i + 1][j - 1] != -2) {

					is_checker_was_attacked = false;
					break;
				}
			}

			i++;
			j--;
		}

		if (is_checker_was_attacked) {

			return true;
		}

		i = x - 1;
		j = y + 1;
		is_checker_was_attacked = false;

		while (i >= 1 && j <= 6) {

			if (!is_checker_was_attacked && (field[i][j] >= -1 && field[i][j] < 12) || (field[i][j] >= 24 && field[i][j] < 36)) {

				break;
			}

			if ((field[i][j] >= 12 && field[i][j] < 24) || field[i][j] >= 36) {

				is_checker_was_attacked = true;

				if (field[i - 1][j + 1] != -2) {

					is_checker_was_attacked = false;
					break;
				}
			}

			i--;
			j++;
		}

		if (is_checker_was_attacked) {

			return true;
		}
	}

	else if (checker < 48) {

		int i = x + 1;
		int j = y + 1;
		bool is_checker_was_attacked = false;

		while (i <= 6 && j <= 6) {

			if (!is_checker_was_attacked && ((field[i][j] >= 12 && field[i][j] < 24) || field[i][j] >= 36)) {

				break;
			}

			if ((field[i][j] >= -1 && field[i][j] < 12) || (field[i][j] >= 24 && field[i][j] < 36)) {

				is_checker_was_attacked = true;

				if (field[i + 1][j + 1] != -2) {

					is_checker_was_attacked = false;
					break;
				}
			}

			i++;
			j++;
		}

		if (is_checker_was_attacked) {
			return true;
		}

		i = x - 1;
		j = y - 1;
		is_checker_was_attacked = false;

		while (i >= 1 && j >= 1) {

			if (!is_checker_was_attacked && ((field[i][j] >= 12 && field[i][j] < 24) || field[i][j] >= 36)) {

				break;
			}

			if ((field[i][j] >= -1 && field[i][j] < 12) || (field[i][j] >= 24 && field[i][j] < 36)) {

				is_checker_was_attacked = true;

				if (field[i - 1][j - 1] != -2) {

					is_checker_was_attacked = false;
					break;
				}
			}

			i--;
			j--;
		}

		if (is_checker_was_attacked) {

			return true;
		}

		i = x + 1;
		j = y - 1;
		is_checker_was_attacked = false;

		while (i <= 6 && j >= 1) {

			if (!is_checker_was_attacked && ((field[i][j] >= 12 && field[i][j] < 24) || field[i][j] >= 36)) {

				break;
			}

			if ((field[i][j] >= -1 && field[i][j] < 12) || (field[i][j] >= 24 && field[i][j] < 36)) {

				is_checker_was_attacked = true;

				if (field[i + 1][j - 1] != -2) {

					is_checker_was_attacked = false;
					break;
				}
			}

			i++;
			j--;
		}

		if (is_checker_was_attacked) {

			return true;
		}

		i = x - 1;
		j = y + 1;
		is_checker_was_attacked = false;

		while (i >= 1 && j <= 6) {

			if (!is_checker_was_attacked && ((field[i][j] >= 12 && field[i][j] < 24) || field[i][j] >= 36)) {

				break;
			}

			if ((field[i][j] >= -1 && field[i][j] < 12) || (field[i][j] >= 24 && field[i][j] < 36)) {

				is_checker_was_attacked = true;

				if (field[i - 1][j + 1] != -2) {

					is_checker_was_attacked = false;
					break;
				}
			}

			i--;
			j++;
		}

		if (is_checker_was_attacked) {
			return true;
		}
	}

	return false;
}

bool Game::are_attack(int step) {

	if (step == 1) {

		if (!whites_are_attack.empty()) {

			return true;
		}
	}

	else {
		if (!blacks_are_attack.empty()) {

			return true;
		}
	}

	return false;
}


bool  Game::is_attacked(int checker, int step, int ox, int oy, int nx, int ny) {

	if (checker < 24) {

		if (step == 2) {

			if (!std::any_of(blacks_are_attack.begin(), blacks_are_attack.end(), [checker](int i) {return i == checker; })) {

				return false;
			}

			if (ox <= 5 && oy <= 5) {
				if (((field[ox + 1][oy + 1] >= 12 && field[ox + 1][oy + 1] < 24) || field[ox + 1][oy + 1] >= 36) && field[ox + 2][oy + 2] == -2 && nx == ox + 2 && ny == oy + 2) {
					return true;
				}
			}

			if (ox >= 2 && oy >= 2) {
				if (((field[ox - 1][oy - 1] >= 12 && field[ox - 1][oy - 1] < 24) || field[ox - 1][oy - 1] >= 36) && field[ox - 2][oy - 2] == -2 && nx == ox - 2 && ny == oy - 2) {
					return true;
				}
			}

			if (ox <= 5 && oy >= 2) {
				if (((field[ox + 1][oy - 1] >= 12 && field[ox + 1][oy - 1] < 24) || field[ox + 1][oy - 1] >= 36) && field[ox + 2][oy - 2] == -2 && nx == ox + 2 && ny == oy - 2) {
					return true;
				}
			}

			if (ox >= 2 && oy <= 5) {
				if (((field[ox - 1][oy + 1] >= 12 && field[ox - 1][oy + 1] < 24) || field[ox - 1][oy + 1] >= 36) && field[ox - 2][oy + 2] == -2 && nx == ox - 2 && ny == oy + 2) {
					return true;
				}
			}
		}
		else if (step == 1) {

			if (!std::any_of(whites_are_attack.begin(), whites_are_attack.end(), [checker](int i) {return i == checker; })) {

				return false;
			}

			if (ox <= 5 && oy <= 5) {
				if (((field[ox + 1][oy + 1] < 12 && field[ox + 1][oy + 1] > -1) || (field[ox + 1][oy + 1] >= 24 && field[ox + 1][oy + 1] < 36)) && field[ox + 2][oy + 2] == -2 && nx == ox + 2 && ny == oy + 2) {
					return true;
				}
			}

			if (ox >= 2 && oy >= 2) {
				if (((field[ox - 1][oy - 1] < 12 && field[ox - 1][oy - 1] > -1) || (field[ox - 1][oy - 1] >= 24 && field[ox - 1][oy - 1] < 36)) && field[ox - 2][oy - 2] == -2 && nx == ox - 2 && ny == oy - 2) {
					return true;
				}
			}

			if (ox <= 5 && oy >= 2) {
				if (((field[ox + 1][oy - 1] < 12 && field[ox + 1][oy - 1] > -1) || (field[ox + 1][oy - 1] >= 24 && field[ox + 1][oy - 1] < 36)) && field[ox + 2][oy - 2] == -2 && nx == ox + 2 && ny == oy - 2) {
					return true;
				}
			}

			if (ox >= 2 && oy <= 5) {
				if (((field[ox - 1][oy + 1] < 12 && field[ox - 1][oy + 1] > -1) || (field[ox - 1][oy + 1] >= 24 && field[ox - 1][oy + 1] < 36)) && field[ox - 2][oy + 2] == -2 && nx == ox - 2 && ny == oy + 2) {
					return true;
				}
			}
		}

		return false;
	}

	else if (checker >= 24 && checker < 48) {

		if (field[nx][ny] != -2) {

			return false;
		}

		if (step == 2) {

			if (!std::any_of(blacks_are_attack.begin(), blacks_are_attack.end(), [checker](int i) {return i == checker; })) {

				return false;
			}

			if (nx > ox && ny > oy) {

				int i = ox + 1;
				int j = oy + 1;
				bool is_checker_was_attacked = false;

				while (i <= nx - 1 && j <= ny - 1) {

					if (is_checker_was_attacked && ((field[i][j] >= 12 && field[i][j] < 24) || field[i][j] >= 36)) {

						return false;
					}

					if ((field[i][j] >= 12 && field[i][j] < 24) || field[i][j] >= 36) {

						is_checker_was_attacked = true;
						i++;
						j++;
						continue;
					}

					i++;
					j++;
				}

				if (is_checker_was_attacked) {

					return true;
				}
			}

			if (nx < ox && ny < oy) {

				int i = ox - 1;
				int j = oy - 1;
				bool is_checker_was_attacked = false;

				while (i >= nx + 1 && j >= ny + 1) {

					if (is_checker_was_attacked && ((field[i][j] >= 12 && field[i][j] < 24) || field[i][j] >= 36)) {

						return false;
					}

					if ((field[i][j] >= 12 && field[i][j] < 24) || field[i][j] >= 36) {

						is_checker_was_attacked = true;
						i--;
						j--;
						continue;
					}

					i--;
					j--;
				}

				if (is_checker_was_attacked) {

					return true;
				}
			}

			if (nx > ox && ny < oy) {

				int i = ox + 1;
				int j = oy - 1;
				bool is_checker_was_attacked = false;

				while (i <= nx - 1 && j >= ny + 1) {

					if (is_checker_was_attacked && ((field[i][j] >= 12 && field[i][j] < 24) || field[i][j] >= 36)) {

						return false;
					}

					if ((field[i][j] >= 12 && field[i][j] < 24) || field[i][j] >= 36) {

						is_checker_was_attacked = true;
						i++;
						j--;
						continue;
					}

					i++;
					j--;
				}
				if (is_checker_was_attacked) {

					return true;
				}
			}

			if (nx < ox && ny > oy) {

				int i = ox - 1;
				int j = oy + 1;
				bool is_checker_was_attacked = false;

				while (i >= nx + 1 && j <= ny - 1) {

					if (is_checker_was_attacked && ((field[i][j] >= 12 && field[i][j] < 24) || field[i][j] >= 36)) {

						return false;
					}

					if ((field[i][j] >= 12 && field[i][j] < 24) || field[i][j] >= 36) {

						is_checker_was_attacked = true;
						i--;
						j++;
						continue;
					}

					i--;
					j++;
				}
				if (is_checker_was_attacked) {

					return true;
				}
			}
		}

		else if (step == 1) {

			if (!std::any_of(whites_are_attack.begin(), whites_are_attack.end(), [checker](int i) {return i == checker; })) {
				return false;
			}

			if (nx > ox && ny > oy) {

				int i = ox + 1;
				int j = oy + 1;
				bool is_checker_was_attacked = false;

				while (i <= nx - 1 && j <= ny - 1) {

					if (is_checker_was_attacked && ((field[i][j] > -1 && field[i][j] < 12) || (field[i][j] >= 24 && field[i][j] < 36))) {

						return false;
					}

					if ((field[i][j] > -1 && field[i][j] < 12) || (field[i][j] >= 24 && field[i][j] < 36)) {

						is_checker_was_attacked = true;
						i++;
						j++;
						continue;
					}

					i++;
					j++;
				}

				if (is_checker_was_attacked) {
					return true;
				}
			}

			if (nx < ox && ny < oy) {

				int i = ox - 1;
				int j = oy - 1;
				bool is_checker_was_attacked = false;

				while (i >= nx + 1 && j >= ny + 1) {

					if (is_checker_was_attacked && ((field[i][j] > -1 && field[i][j] < 12) || (field[i][j] >= 24 && field[i][j] < 36))) {

						return false;
					}

					if ((field[i][j] > -1 && field[i][j] < 12) || (field[i][j] >= 24 && field[i][j] < 36)) {

						is_checker_was_attacked = true;
						i--;
						j--;
						continue;
					}

					i--;
					j--;
				}

				if (is_checker_was_attacked) {
					return true;
				}
			}

			if (nx > ox && ny < oy) {

				int i = ox + 1;
				int j = oy - 1;
				bool is_checker_was_attacked = false;

				while (i <= nx - 1 && j >= ny + 1) {

					if (is_checker_was_attacked && ((field[i][j] > -1 && field[i][j] < 12) || (field[i][j] >= 24 && field[i][j] < 36))) {

						return false;
					}

					if ((field[i][j] > -1 && field[i][j] < 12) || (field[i][j] >= 24 && field[i][j] < 36)) {

						is_checker_was_attacked = true;
						i++;
						j--;
						continue;
					}

					i++;
					j--;
				}

				if (is_checker_was_attacked) {
					return true;
				}
			}

			if (nx < ox && ny > oy) {

				int i = ox - 1;
				int j = oy + 1;
				bool is_checker_was_attacked = false;

				while (i >= nx + 1 && j <= ny - 1) {

					if (is_checker_was_attacked && ((field[i][j] > -1 && field[i][j] < 12) || (field[i][j] >= 24 && field[i][j] < 36))) {

						return false;
					}

					if ((field[i][j] > -1 && field[i][j] < 12) || (field[i][j] >= 24 && field[i][j] < 36)) {

						is_checker_was_attacked = true;
						i--;
						j++;
						continue;
					}

					i--;
					j++;
				}
				if (is_checker_was_attacked) {
					return true;
				}
			}
		}
	}
}

void Game::attacked(int checker, int step, int ox, int oy, int nx, int ny) {

	if (checker < 24) {

		if (step == 2) {

			if (ox <= 5 && oy <= 5) {
				if (((field[ox + 1][oy + 1] >= 12 && field[ox + 1][oy + 1] < 24) || field[ox + 1][oy + 1] >= 36) && field[ox + 2][oy + 2] == -2 && nx == ox + 2 && ny == oy + 2) {
					f[field[ox + 1][oy + 1]].setPosition(-100, -100);
					field[ox][oy] = -2;
					field[ox + 1][oy + 1] = -2;
					field[ox + 2][oy + 2] = checker;
				}
			}

			if (ox >= 2 && oy >= 2) {
				if (((field[ox - 1][oy - 1] >= 12 && field[ox - 1][oy - 1] < 24) || field[ox - 1][oy - 1] >= 36) && field[ox - 2][oy - 2] == -2 && nx == ox - 2 && ny == oy - 2) {
					f[field[ox - 1][oy - 1]].setPosition(-100, -100);
					field[ox][oy] = -2;
					field[ox - 1][oy - 1] = -2;
					field[ox - 2][oy - 2] = checker;
				}
			}

			if (ox <= 5 && oy >= 2) {
				if (((field[ox + 1][oy - 1] >= 12 && field[ox + 1][oy - 1] < 24) || field[ox + 1][oy - 1] >= 36) && field[ox + 2][oy - 2] == -2 && nx == ox + 2 && ny == oy - 2) {
					f[field[ox + 1][oy - 1]].setPosition(-100, -100);
					field[ox][oy] = -2;
					field[ox + 1][oy - 1] = -2;
					field[ox + 2][oy - 2] = checker;
				}
			}

			if (ox >= 2 && oy <= 5) {
				if (((field[ox - 1][oy + 1] >= 12 && field[ox - 1][oy + 1] < 24) || field[ox - 1][oy + 1] >= 36) && field[ox - 2][oy + 2] == -2 && nx == ox - 2 && ny == oy + 2) {
					f[field[ox - 1][oy + 1]].setPosition(-100, -100);
					field[ox][oy] = -2;
					field[ox - 1][oy + 1] = -2;
					field[ox - 2][oy + 2] = checker;
				}
			}
		}
		else if (step == 1) {

			if (ox <= 5 && oy <= 5) {
				if (((field[ox + 1][oy + 1] < 12 && field[ox + 1][oy + 1] > -1) || (field[ox + 1][oy + 1] >= 24 && field[ox + 1][oy + 1] < 36)) && field[ox + 2][oy + 2] == -2 && nx == ox + 2 && ny == oy + 2) {
					f[field[ox + 1][oy + 1]].setPosition(-100, -100);
					field[ox][oy] = -2;
					field[ox + 1][oy + 1] = -2;
					field[ox + 2][oy + 2] = checker;
				}
			}

			if (ox >= 2 && oy >= 2) {
				if (((field[ox - 1][oy - 1] < 12 && field[ox - 1][oy - 1] > -1) || (field[ox - 1][oy - 1] >= 24 && field[ox - 1][oy - 1] < 36)) && field[ox - 2][oy - 2] == -2 && nx == ox - 2 && ny == oy - 2) {
					f[field[ox - 1][oy - 1]].setPosition(-100, -100);
					field[ox][oy] = -2;
					field[ox - 1][oy - 1] = -2;
					field[ox - 2][oy - 2] = checker;
				}
			}

			if (ox <= 5 && oy >= 2) {
				if (((field[ox + 1][oy - 1] < 12 && field[ox + 1][oy - 1] > -1) || (field[ox + 1][oy - 1] >= 24 && field[ox + 1][oy - 1] < 36)) && field[ox + 2][oy - 2] == -2 && nx == ox + 2 && ny == oy - 2) {
					f[field[ox + 1][oy - 1]].setPosition(-100, -100);
					field[ox][oy] = -2;
					field[ox + 1][oy - 1] = -2;
					field[ox + 2][oy - 2] = checker;
				}
			}

			if (ox >= 2 && oy <= 5) {
				if (((field[ox - 1][oy + 1] < 12 && field[ox - 1][oy + 1] > -1) || (field[ox - 1][oy + 1] >= 24 && field[ox - 1][oy + 1] < 36)) && field[ox - 2][oy + 2] == -2 && nx == ox - 2 && ny == oy + 2) {
					f[field[ox - 1][oy + 1]].setPosition(-100, -100);
					field[ox][oy] = -2;
					field[ox - 1][oy + 1] = -2;
					field[ox - 2][oy + 2] = checker;
				}
			}
		}
	}

	else {

		field[ox][oy] = -2;
		field[nx][ny] = checker;

		if (step == 2) {

			if (nx > ox && ny > oy) {

				int i = ox + 1;
				int j = oy + 1;

				while (i <= nx - 1 && j <= ny - 1) {

					if ((field[i][j] >= 12 && field[i][j] < 24) || field[i][j] >= 36) {

						f[field[i][j]].setPosition(-100, -100);
						field[i][j] = -2;
					}

					i++;
					j++;
				}
			}

			if (nx < ox && ny < oy) {

				int i = ox - 1;
				int j = oy - 1;

				while (i >= nx + 1 && j >= ny + 1) {

					if ((field[i][j] >= 12 && field[i][j] < 24) || field[i][j] >= 36) {

						f[field[i][j]].setPosition(-100, -100);
						field[i][j] = -2;
					}

					i--;
					j--;
				}

			}

			if (nx > ox && ny < oy) {

				int i = ox + 1;
				int j = oy - 1;

				while (i <= nx - 1 && j >= ny + 1) {

					if ((field[i][j] >= 12 && field[i][j] < 24) || field[i][j] >= 36) {

						f[field[i][j]].setPosition(-100, -100);
						field[i][j] = -2;
					}

					i++;
					j--;
				}
			}

			if (nx < ox && ny > oy) {

				int i = ox - 1;
				int j = oy + 1;

				while (i >= nx + 1 && j <= ny - 1) {

					if ((field[i][j] >= 12 && field[i][j] < 24) || field[i][j] >= 36) {

						f[field[i][j]].setPosition(-100, -100);
						field[i][j] = -2;
					}

					i--;
					j++;
				}
			}
		}

		else if (step == 1) {

			if (nx > ox && ny > oy) {

				int i = ox + 1;
				int j = oy + 1;

				while (i <= nx - 1 && j <= ny - 1) {

					if ((field[i][j] > -1 && field[i][j] < 12) || (field[i][j] >= 24 && field[i][j] < 36)) {

						f[field[i][j]].setPosition(-100, -100);
						field[i][j] = -2;
					}

					i++;
					j++;
				}
			}

			if (nx < ox && ny < oy) {

				int i = ox - 1;
				int j = oy - 1;

				while (i >= nx + 1 && j >= ny + 1) {

					if ((field[i][j] > -1 && field[i][j] < 12) || (field[i][j] >= 24 && field[i][j] < 36)) {

						f[field[i][j]].setPosition(-100, -100);
						field[i][j] = -2;
					}

					i--;
					j--;
				}
			}

			if (nx > ox && ny < oy) {

				int i = ox + 1;
				int j = oy - 1;

				while (i <= nx - 1 && j >= ny + 1) {

					if ((field[i][j] > -1 && field[i][j] < 12) || (field[i][j] >= 24 && field[i][j] < 36)) {

						f[field[i][j]].setPosition(-100, -100);
						field[i][j] = -2;
					}

					i++;
					j--;
				}
			}

			if (nx < ox && ny > oy) {

				int i = ox - 1;
				int j = oy + 1;

				while (i >= nx + 1 && j <= ny - 1) {

					if ((field[i][j] > -1 && field[i][j] < 12) || (field[i][j] >= 24 && field[i][j] < 36)) {

						f[field[i][j]].setPosition(-100, -100);
						field[i][j] = -2;
					}

					i--;
					j++;
				}
			}
		}
	}
}

void Game::updateVectorOfAttackingCheckers() {

	whites_are_attack.clear();
	blacks_are_attack.clear();

	for (int i = 0; i < 8; i++) {

		for (int j = 0; j < 8; j++) {

			if (field[i][j] > -1 && is_attack(field[i][j], i, j)) {

				if (field[i][j] < 12 || (field[i][j] >= 24 && field[i][j] < 36)) {

					blacks_are_attack.push_back(field[i][j]);
				}

				else {

					whites_are_attack.push_back(field[i][j]);
				}
			}
		}
	}
}

void Game::makeACheckerKing(int &n) {

	int on = n;
	n += 24;

	for (int i = 0; i < 8; i++) {

		for (int j = 0; j < 8; j++) {

			if (field[i][j] == on) {

				field[i][j] = n;
			}
		}
	}

	f[on].setPosition(-100, -100);
}

int Game::someone_wins() {

	std::vector<int> blacks;
	std::vector<int> whites;

	for (int i = 0; i < 8; i++) {

		for (int j = 0; j < 8; j++) {

			if ((field[i][j] > -1 && field[i][j] < 12) || (field[i][j] >= 24 && field[i][j] < 36)) {

				blacks.push_back(field[i][j]);
			}

			else if ((field[i][j] >= 12 && field[i][j] < 24) || field[i][j] >= 36) {

				whites.push_back(field[i][j]);
			}
		}
	}

	if (whites.empty()) {

		return 2;
	}

	else if (blacks.empty()) {

		return 1;
	}

	else {

		return 0;
	}
}
