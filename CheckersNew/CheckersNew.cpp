﻿#include <iostream>
#include <vector>
#include <SFML/Graphics.hpp>
#include "Game.h"
#include <time.h>

using namespace sf;

int main() {

	RenderWindow window(VideoMode(400, 400), "Checkers");

	Game game;

	game.load_textures();
	game.load_position();
	Sprite Board(game.t3);

	bool isMove = false;
	float dx = 0, dy = 0;
	int n = 0;
	int attacker = -1;
	float oldPosX = 0, oldPosY = 0;

	while (window.isOpen()) 
	{
		Vector2i pos = Mouse::getPosition(window);

		Event event;

		while (window.pollEvent(event)) {

			if (event.type == Event::Closed)
				window.close();

			if (event.type == Event::MouseButtonPressed) {

				if (event.key.code == Mouse::Left) {

					for (int i = 0; i < 48; i++) {

						if (game.f[i].getGlobalBounds().contains(pos.x, pos.y)) {
							isMove = true;  n = i;
							oldPosX = game.f[i].getPosition().x;
							oldPosY = game.f[i].getPosition().y;
							dx = pos.x - game.f[i].getPosition().x;
							dy = pos.y - game.f[i].getPosition().y;
						}
					}
				}
			}

			if (event.type == Event::Resized) {

				FloatRect visibleArea(0.f, 0.f, event.size.width, event.size.height);
				window.setView(View(visibleArea));
			}

			if (event.type == Event::MouseButtonReleased) {

				if (event.key.code == Mouse::Left) {

					isMove = false;
					Vector2f p = game.f[n].getPosition() + Vector2f(game.size / 2, game.size / 2);

					int ox, oy;

					for (int i = 0; i < 8; i++) {

						for (int j = 0; j < 8; j++) {

							if (game.field[i][j] == n) {

								ox = i, oy = j;
								break;
							}
						}
					}

					if (game.are_attack(game.step)) {

						std::cout << "is_attack" << std::endl;

						if (!game.is_attacked(n, game.step, ox, oy, int(p.y/game.size), int(p.x/game.size))) {

							game.f[n].setPosition(oy * game.size, ox * game.size);
						}
						
						else {

							game.attacked(n, game.step, ox, oy, int(p.y / game.size), int(p.x / game.size));

							if ((n >= 12 && n < 24 && int(p.y / game.size) == 0) || (n < 12 && int(p.y / game.size) == 7)) {

								game.makeACheckerKing(n);
							}
							std::cout << n << std::endl;
							Vector2f newPos = Vector2f(game.size * int(p.x / game.size), game.size * int(p.y / game.size));
							game.f[n].setPosition(newPos);

							game.whites_are_attack.clear();
							game.blacks_are_attack.clear();

							if (!game.is_attack(n, int(p.y / game.size), int(p.x / game.size))) {

								if (game.step == 1)
									game.step = 2;
								else
									game.step = 1;

								game.updateVectorOfAttackingCheckers();
							}

							else {
								if (game.step == 1)
									game.whites_are_attack.push_back(n);
								else
									game.blacks_are_attack.push_back(n);
							}
							
						}
					}
					else if (!game.legal_move(n, game.step, int(p.y / game.size), int(p.x / game.size), ox, oy)) {

						game.f[n].setPosition(oy * game.size, ox * game.size);
					} 

					else {

						if (n < 24) {

							if ((n >= 12 && int(p.y / game.size) == 0 || (n < 12 && int(p.y / game.size) == 7))) {

								game.makeACheckerKing(n);
							}
						}

						Vector2f newPos = Vector2f(game.size * int(p.x / game.size), game.size * int(p.y / game.size));
						game.f[n].setPosition(newPos);

						for (int i = 0; i < 8; i++) {

							for (int j = 0; j < 8; j++) {

								if (game.field[i][j] == n) {

									game.field[i][j] = -2;
								}
							}
						}

						if (game.step == 1)
							game.step = 2;
						else
							game.step = 1;

						game.field[int(p.y / game.size)][int(p.x / game.size)] = n;

						game.updateVectorOfAttackingCheckers();
					}
				}
			}
		}

		if (isMove) game.f[n].setPosition(pos.x - dx, pos.y - dy);

		window.clear();
		window.draw(Board);

		for (int i = 0; i < 48; i++) {

			window.draw(game.f[i]);
		}

		window.display();

		if (game.someone_wins() != 0) {

			window.close();
		}
	}

	RenderWindow window2(VideoMode(400, 400), "checkers");
	Texture t11;
	t11.loadFromFile("Images/white_won.png");
	Sprite white_won(t11);
	Texture t12;
	t12.loadFromFile("Images/black_won.png");
	Sprite black_won(t12);

	if (game.someone_wins() == 1) {

		window2.draw(white_won);
		window2.display();
	}

	else if (game.someone_wins() == 2) {

		window2.draw(black_won);
		window2.display();
	}

	while (window2.isOpen()) {

		Event event;

		while (window2.pollEvent(event)) {

			if (event.type == Event::Closed) {

				window2.close();
			}
		}
	}
}
